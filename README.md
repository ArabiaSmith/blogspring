- # Curso Java Cloud #

* Curso Superior impartido por Core Networks Sevilla.
* [Link al contenido](https://www.corenetworks.es/cursos-superiores/curso-superior-programacion-aplicaciones-tecnologia-java-cloud/)

* Aprendo a trabajar con los IDE: Netbeans y Spring Tool Suite.
* El contenido del curso se centra en el desarrollo del material oficial de Oracle JAVA SE 8 Fundamentals y Programming:
* Streams, filtros, expresiones lambda, JDBC con MySQL, Struts, Spring, Hibernate, Netbeans, JSP, POJO, EJB,etc.

* Tambi�n conozco un poco de HTML y CSS que he aprendido por m� misma.
* Tengo un nivel medio de Photoshop ya que siempre me ha gustado bastante.